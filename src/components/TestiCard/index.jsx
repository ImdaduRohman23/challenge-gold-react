import { Grid, Card } from "@mui/material";
import './style.css'
import imgtesti1 from '../../assets/img-testi1.svg'
import imgtesti2 from '../../assets/img-testi2.svg'
import imgtesti3 from '../../assets/img-testi3.svg'

const TestiCard = () => {
    return (
        <>
        <div className="bungkus-testi">
            <div className="card-testi">
                <div className="img-testi">
                    <img src={imgtesti1} alt="" />
                </div>
                <div className="konten-testi">
                    <img src={imgtesti3} alt="" />
                    <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                    <p>John Dee 32, Bromo</p>
                </div>
            </div>

            <div className="card-testi">
                <div className="img-testi">
                    <img src={imgtesti2} alt="" />
                </div>
                <div className="konten-testi">
                    <img src={imgtesti3} alt="" />
                    <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                    <p>John Dee 32, Bromo</p>
                </div>
            </div>
        </div>
        </>
    )
}

export default TestiCard;