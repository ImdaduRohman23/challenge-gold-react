import './style.css'
import ListFaqDesk from '../../assets/list-faq.svg'
import ListFaqMobile from '../../assets/list-faq-mobile.svg'

import AccordionFaq from '../AccordionFaq/AccordionFaq'

export default function Section6Faq() {
    return(
        <section class="faq" id='faq'>
            <div class="faq-kiri">
                <h3>Frequently Asked Question</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>
            <div class="faq-kanan">
            <AccordionFaq /> 
            </div>            
        </section>

    )
}