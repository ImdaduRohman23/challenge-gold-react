import './style.css'
import { Link } from 'react-router-dom'

export default function Section5Banner() {
    return(
        <section class="banner">
            <h3>Sewa Mobil di (Lokasimu) Sekarang</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua. </p>
            <button className='button-hero'>
                    <Link to="/searchcar">Mulai Sewa Mobil</Link>
            </button>
        </section>

    )
}