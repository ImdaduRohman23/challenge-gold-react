import './style.css'
import ListSosmed from '../../assets/list-sosmed.svg'

export default function Section7Footer() {
    return(
        <footer>
            <div class="addres">
                <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
            </div>
            <div class="nav">
                <ul>
                    <li>Our Services</li>
                    <li>Why Us</li>
                    <li>Testimonial</li>
                    <li>FAQ</li>
                </ul>
            </div>
            <div class="sosmed">
                <p>Connect with us</p>
                <img src={ListSosmed} alt="" />
                {/* <img src="image/sosmedlogo.png" alt=""> */}
            </div>
            <div class="coppyright">
                <p>Copyright Binar 2022</p>
            </div>
    </footer>

    )
}