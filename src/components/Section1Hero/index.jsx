import './style.css'
import img1 from '../../assets/1.svg'
import { Link } from 'react-router-dom';


function Section1Hero() {
    return(
        <div class="hero-header">
            <div class="konten-hero">
                <h1>Sewa dan Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau.
                    Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                <button className='button-hero'>
                    <Link to="/searchcar">Mulai Sewa Mobil</Link>
                </button>
            </div>
            <div class="image-hero">
                <img src={img1} alt="" />
            </div>
        </div>
    )
}

export default Section1Hero;