import './style.css'
import img2 from '../../assets/2.svg'
import img3 from '../../assets/3.svg'

function Section2Services() {
    return(
        <section class="ourServices" id="ourServices">
            <div class="image-ourServices">
                <img src={img2} alt="" />
            </div>

            <div class="desc-ourServices">
                <h3>Best Car Rental for any kind of trip in (Lokasimu)!</h3>
                <p>
                    Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
                    kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting,
                    dll.
                </p>
                <div>
                    <img src={img3} alt="" />
                </div>
            </div>
        </section>

    )
}

export default Section2Services;