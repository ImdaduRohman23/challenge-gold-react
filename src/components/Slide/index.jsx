import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import React from "react";
import Slider from "react-slick";

import './style.css'
import imgtesti1 from '../../assets/img-testi1.svg'
import imgtesti2 from '../../assets/img-testi2.svg'
import imgtesti3 from '../../assets/img-testi3.svg'


const Slide = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2.5,
        slidesToScroll: 1
};
        return (
            <Slider {...settings}>
                <div className="bungkus-testi">
                    <div className="card-testi">
                        <div className="img-testi">
                            <img src={imgtesti1} alt="" />
                        </div>
                        <div className="konten-testi">
                            <img src={imgtesti3} alt="" />
                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p>John Dee 32, Bromo</p>
                        </div>
                    </div>
                </div>

                <div className="bungkus-testi">
                    <div className="card-testi">
                        <div className="img-testi">
                            <img src={imgtesti2} alt="" />
                        </div>
                        <div className="konten-testi">
                            <img src={imgtesti3} alt="" />
                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p>John Dee 32, Bromo</p>
                        </div>
                    </div>
                </div>

                <div className="bungkus-testi">
                    <div className="card-testi">
                        <div className="img-testi">
                            <img src={imgtesti1} alt="" />
                        </div>
                        <div className="konten-testi">
                            <img src={imgtesti3} alt="" />
                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <p>John Dee 32, Bromo</p>
                        </div>
                    </div>
                </div>
            </Slider>
    )
}

export default Slide;