import { Skeleton, Card, Typography, Grid } from "@mui/material";

const SkeletonDetail= () => {
    return (
            <Grid container justifyContent='Center' spacing={2}>
                <Grid item>
                    <Card  sx={{width: '500px', padding: '20px'}}>
                        <Typography variant='h6' marginBottom={2}>
                            <Skeleton variant="text" width={210}/>                
                        </Typography>
                        <Typography variant='p'>
                            <Skeleton variant="text" width={210}/>
                        </Typography>
                        <br></br><br></br>
                            <Skeleton variant="rectangular" width={210} height={124} />
                        <br></br><br></br>
                        <Typography variant='p'>
                            <Skeleton variant="text" width={210}/>
                        </Typography>
                        <br></br><br></br>
                            <Skeleton variant="rectangular" width={210} height={124} />
                        <Typography variant='h6' marginTop={2} marginBottom={2}>
                            <Skeleton variant="text" width={210}/>
                        </Typography>
                            <Skeleton variant="rectangular" width={210} height={124} />
                    </Card>
                </Grid>
                <Grid item xs={12} md={4} lg={4} >
                    <Card sx={{padding: '20px'}}>
                        <Skeleton variant="rectangular" width={210} height={124} />                 
                    <Typography variant="h5">
                        <Skeleton variant="text" width={210}/>
                    </Typography>
                    <p>
                        <Skeleton variant="text" width={210}/>
                    </p>
                        <Skeleton variant="text" width={'100%'} height={25}/>
                    </Card>
                </Grid>
            </Grid>

    )
}

export default SkeletonDetail;