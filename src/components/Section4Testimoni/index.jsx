import './style.css'
import TestiCard from '../TestiCard'
import Slide from '../Slide'
import CustomArrows from '../SimpleSilder'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function Section4Testimoni() {
    return(
        <section class="testimoni" id='testimoni'>
            <div class="kalimat1">Testimonial</div>
            <div class="kalimat2">Berbagai review positif dari para pelanggan kami</div>
            <div class="testimoni-list">
            <Slide />            
                {/* <div class="testimobile">
                <TestiCard />
                </div> */}
                {/* <div class="testidekstop">
                <TestiCard />
                </div> */}
            </div>
            <div class="testimoni-buttonCarousel">
                <div class="buttonCarousel-kiri">
                    {/* <img src="image/testilb.png" alt=""> */}
                </div>
                <div class="buttonCarousel-kanan">
                    {/* <img src="image/testirb.png" alt=""> */}
                </div>
            </div>
        </section>

    )
}