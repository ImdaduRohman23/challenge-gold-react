import { Accordion, AccordionSummary, AccordionDetails, Typography } from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import './style.css'

const AccordionFaq = () => {
    return (
        <div>
            <Accordion>
                <AccordionSummary 
                id="panel1-header" 
                aria-controls="panel1-content" 
                expandIcon={<ExpandMoreIcon/ >}>
                    <Typography className="conten-accordion">Apa saja syarat yang dibutuhkan?</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe ipsum earum officia illo minima tempora assumenda, nesciunt corrupti totam molestias, distinctio, non deserunt iure ullam enim dolore nostrum repellat dolor.
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary 
                id="panel2-header" 
                aria-controls="panel2-content" 
                expandIcon={<ExpandMoreIcon/ >}>
                    <Typography className="conten-accordion">Berapa hari minimal sewa mobil lepas kunci?</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe ipsum earum officia illo minima tempora assumenda, nesciunt corrupti totam molestias, distinctio, non deserunt iure ullam enim dolore nostrum repellat dolor.
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary 
                id="panel2-header" 
                aria-controls="panel2-content" 
                expandIcon={<ExpandMoreIcon/ >}>
                    <Typography className="conten-accordion">Berapa hari sebelumnya sabaiknya booking sewa mobil?</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe ipsum earum officia illo minima tempora assumenda, nesciunt corrupti totam molestias, distinctio, non deserunt iure ullam enim dolore nostrum repellat dolor.
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary 
                id="panel2-header" 
                aria-controls="panel2-content" 
                expandIcon={<ExpandMoreIcon/ >}>
                    <Typography className="conten-accordion">Apakah Ada biaya antar-jemput?</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe ipsum earum officia illo minima tempora assumenda, nesciunt corrupti totam molestias, distinctio, non deserunt iure ullam enim dolore nostrum repellat dolor.
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <Accordion>
                <AccordionSummary 
                id="panel2-header" 
                aria-controls="panel2-content" 
                expandIcon={<ExpandMoreIcon/ >}>
                    <Typography className="conten-accordion">Bagaimana jika terjadi kecelakaan</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe ipsum earum officia illo minima tempora assumenda, nesciunt corrupti totam molestias, distinctio, non deserunt iure ullam enim dolore nostrum repellat dolor.
                    </Typography>
                </AccordionDetails>
            </Accordion>

        </div>
    )
}

export default AccordionFaq;