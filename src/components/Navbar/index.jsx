import { Link } from 'react-router-dom';
import './style.css'
import '../Section2Services/index'
import '../Section3WhyUs/index'
import '../Section4Testimoni/index'
import '../Section6Faq/index'
import { useState } from 'react';

// https://codesandbox.io/s/material-ui-navbar-responsive-lf30l?from-embed=&resolutionWidth=320&resolutionHeight=675


export default function Navbar() {
    const [click, setClick] = useState(false)
    const handleClick = () => setClick(!click);


    return (
        <>
        {/* <Link to="/">Page Landing</Link>
        <Link to="/searchcar">Search Car</Link>
        <Link to="/carlist">Car List</Link>
        <Link to="/detailcar">Detail Car</Link> */}
        
        <header>
            <nav className='nav-header'>
                <div class="logo-nav"></div>
                <div className={click ? "navigation-nav active" : "navigation-nav"}>
                    <ul class="list-navigation">
                        <li><a href="#ourServices">Our Service</a></li>
                        <li><a href="#whyUs">Why Us</a></li>
                        <li><a href="#testimoni">Testimonial</a></li>
                        <li><a href="#faq">FAQ</a></li>
                    </ul>
                </div>
                <div className='nav-icon humberger-nav' onClick={handleClick}>
                    <i className={click ? 'fas fa-times' : 'fas fa-bars'}></i>
                </div>
            </nav>
        </header>
        </>
    )
}