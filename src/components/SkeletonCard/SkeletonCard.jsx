import { Skeleton, Card } from "@mui/material";

const SkeletonCard = () => {
    return (
        <Card sx={{ maxWidth: 345,
            height: '400px', 
            padding: '20px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: '',
            justifyContent: 'space-between',
            }}>

            <Skeleton variant="rectangular" width={210} height={124} />                 
            <Skeleton variant="text" width={210}/>
            <Skeleton variant="text" width={210}/>
            <Skeleton variant="text" width={210}/>
            <Skeleton variant="text" width={'100%'} height={25}/>
        </Card>
    )
}

export default SkeletonCard;