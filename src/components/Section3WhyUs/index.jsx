import './style.css'
import img4a from '../../assets/4a.svg'
import img4b from '../../assets/4b.svg'
import img4c from '../../assets/4c.svg'
import img4d from '../../assets/4d.svg'


export default function Section3WhyUs() {
    return(
        <section class="whyUs" id='whyUs'>
            <h3 class="konten1-whyUs">
                Why Us?
            </h3>
            <p class="konten2-whyUs">
                Mengapa harus pilih Binar Car Rental?
            </p>
            <div class="konten3-whyUs">
                <div class="konten-whyus3-a kontenwy3all">
                    <img src={img4a} alt="" />
                </div>
                <div class="konten-whyus3-b kontenwy3all">
                    <img src={img4b} alt="" />
                </div>
                <div class="konten-whyus3-c kontenwy3all">
                    <img src={img4c} alt="" />
                </div>
                <div class="konten-whyus3-d kontenwy3all">
                    <img src={img4d} alt="" />
                </div>
            </div>
    </section>
    )
}