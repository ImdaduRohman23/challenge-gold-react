import {useState, useEffect} from 'react';
import Section1Hero from '../../components/Section1Hero';
const URL = 'https://bootcamp-rent-car.herokuapp.com/admin/car'
import './style.css'
import { Link, useNavigate } from 'react-router-dom';

// import Button from '@mui/material/Button';
import { Grid, Button, Card, Typography, Box } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { bgcolor } from '@mui/system';
const imageUrl = 'https://firebasestorage.googleapis.com/v0/b/rent-car-507f6.appspot.com/o/1655268790869-Innova.png?alt=media';

import SkeletonCard from '../../components/SkeletonCard/SkeletonCard';

function CarList() {
    const [cars, setCars] = useState([]);
    const [loading, setLoading] = useState(false)

    let navigate = useNavigate();

    async function getCars(){
        try{
            setLoading(true)
            const res = await fetch(URL);
            const data = await res.json();
            setCars(data)
            setLoading(false)
        } catch(e){
            setLoading(false)
            console.log(e)
        }
    }
    useEffect(() => {
        getCars()
    }, [])

    function handleViewDetail(id){
        navigate(`/carlist/${id}`)
    }

    const skeletonLength = [1, 2, 3];

    
    return(
        <>
            <div className="space"></div>

            <div className="kotakCari">
                <div>
                    <form action="">
                        <label for="namaMobil">Nama Mobil</label>
                        <br />
                        <input name="namaMobil" id="namaMobil" className='cars' placeholder='Ketik nama/tipe mobil'/>
                    </form>
                </div>

                <form>
                </form>

                <div className="kategori">
                    <form action="">
                        <label for="kategori">Kategori</label>
                        <br />
                        <select name="kategori" id="kategori" className='cars'>
                            <option value="volvo">Masukan Kapasitas Mobil</option>
                            <option value="saab">2-4 Orang</option>
                            <option value="opel">4-6 Orang</option>
                            <option value="audi">6-8 Orang</option>
                        </select>
                    </form>
                </div>

                <div className="ambil">
                    <form action="">
                        <label for="ambil">Waktu Jemput/Ambil:</label>
                        <br />
                        <select name="ambil" id="ambil" className='cars'>
                            <option value="volvo">Pilih Waktu</option>
                            <option value="saab">Saab</option>
                            <option value="opel">Opel</option>
                            <option value="audi">Audi</option>
                        </select>
                    </form>

                </div>
                <div className="jumlahPenumpnag">
                    <form action="">
                        <label for="jumlahPenumpnag">Status</label>
                        <br />
                        <select name="jumlahPenumpnag" id="jumlahPenumpnag" className='cars'>
                            <option value="volvo">Disewa</option>
                            <option value="opel">Opel</option>
                            <option value="audi">Audi</option>
                        </select>
                    </form>
                </div>
                <button className='buttonEdit'>
                    <Link to="/searchcar">Edit</Link>
                </button>
            </div>

                

            {loading && (
                <Grid container justifyContent='center' p={10} spacing={3}>
                    {skeletonLength.map(car => (
                        <Grid item xs={12} md={6} lg={4} sx={{ marginBottom: '20px'}}>
                            <SkeletonCard />
                        </Grid>
                    ))}
                </Grid>
            )}
            <Grid container justifyContent='center' p={10} spacing={3}>
                {cars.map(car => (
                    <Grid item xs={12} md={6} lg={4} sx={{ marginBottom: '20px'}}>
                        <Card sx={{ maxWidth: 345,
                            height: '400px', 
                            padding: '20px',
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: '',
                            justifyContent: 'space-between',
                            }}>
                            <Box>
                                <img src={car.image ? car.image : imageUrl} alt="cars" 
                                style={{ width: '100%' }}
                                />
                            </Box>
                                
                            <Typography variant="h5">
                                {car.name}
                            </Typography>

                            <Typography variant='h6' >
                                Rp {car.price}/hari
                            </Typography>

                            <Typography variant="p">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            </Typography>
                            <div>
                                <Button variant="contained" color="success" onClick={() => handleViewDetail(car.id)} sx={{width: '100%'}}>Pilih Mobil</Button>
                            </div>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </>
        
    )
}

export default CarList;