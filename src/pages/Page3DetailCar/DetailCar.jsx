import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
const URL = 'https://bootcamp-rent-car.herokuapp.com/admin/car'
import { Grid, Card, Container, Typography, Button } from '@mui/material';
import Detail1 from '../../assets/detail1.svg'
import Detail2 from '../../assets/detail2.svg'
import Detail3 from '../../assets/detail3.svg'
const imageUrl = 'https://firebasestorage.googleapis.com/v0/b/rent-car-507f6.appspot.com/o/1655268790869-Innova.png?alt=media';

import SkeletonDetail from '../../components/SkeletonDetail';

import DrawerAppBar from '../../components/NavbarMui';

function DetailCar() {
    const [car, setCar] = useState('');
    const [loading, setLoading] = useState(false)
    const {id} = useParams();


    async function getCar(){
        try{
            setLoading(true)
            const res = await window.fetch(`${URL}/${id}`);
            const data = await res.json();
            setCar(data)
            setLoading(false)
        } catch(e){
            setLoading(false)
            console.log(e)
        }
    }

    useEffect(() => {
        getCar()
    }, [])

    return(
        <>
            <Container>
                {loading && (
                    <SkeletonDetail />
                )}
                
                <Grid container justifyContent='Center' spacing={2}>
                    <Grid item>
                        <Card  sx={{width: '500px', padding: '20px'}}>
                            <Typography variant='h6' marginBottom={2}>
                                Tentang Paket
                            </Typography>
                            <Typography variant='p'>
                                Include
                            </Typography>
                            <br></br><br></br>
                            <img src={Detail1} alt=""/>
                            <br></br><br></br>
                            <Typography variant='p'>
                                Exculde
                            </Typography>
                            <br></br><br></br>
                            <img src={Detail2} alt="" />
                            <Typography variant='h6' marginTop={2} marginBottom={2}>
                                Refund, Reschedule, Overtime
                            </Typography>
                            <img src={Detail3} alt="" />
                        </Card>
                    </Grid>
                    <Grid item xs={12} md={4} lg={4} >
                        <Card sx={{padding: '20px'}}>
                            <img src={car.image ? car.image : imageUrl} alt="cars" style={{ width: '100%' }}/>
                            <Typography variant="h5">
                                <b>{car.name}</b>
                            </Typography>
                            <p>
                                Total <b>Rp {car.price}</b>
                            </p>
                            <Button variant="contained" color="success" sx={{width: '100%'}}>Lanjutkan Pembayaran</Button>

                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}

export default DetailCar;