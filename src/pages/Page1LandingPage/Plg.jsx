import Section1Hero from "../../components/Section1Hero";
import Section2Services from "../../components/Section2Services";
import Section3WhyUs from "../../components/Section3WhyUs";
import Section4Testimoni from "../../components/Section4Testimoni";
import Section5Banner from "../../components/Section5Banner";
import Section6Faq from "../../components/Section6Faq";

function Page1LandingPage() {
    return(
        <div>
            <Section1Hero />
            <Section2Services />
            <Section3WhyUs />
            <Section4Testimoni />
            <Section5Banner />
            <Section6Faq />
        </div>
        
    )
}

export default Page1LandingPage;