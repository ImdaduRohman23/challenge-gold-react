import './style.css'
import Section1Hero from '../../components/Section1Hero';
import { Link } from 'react-router-dom';


function SearchCar() {
    return(
        <div className="searchCar">
            <Section1Hero />
            <div className="kotakCari">
                <div>
                    <form action="">
                        <label for="namaMobil">Nama Mobil</label>
                        <br />
                        <input name="namaMobil" id="namaMobil" className='cars' placeholder='Ketik nama/tipe mobil'/>
                    </form>
                </div>

                <form>
                </form>

                <div className="kategori">
                    <form action="">
                        <label for="kategori">Kategori</label>
                        <br />
                        <select name="kategori" id="kategori" className='cars'>
                            <option value="volvo">Masukan Kapasitas Mobil</option>
                            <option value="saab">2-4 Orang</option>
                            <option value="opel">4-6 Orang</option>
                            <option value="audi">6-8 Orang</option>
                        </select>
                    </form>
                </div>

                <div className="ambil">
                    <form action="">
                        <label for="ambil">Waktu Jemput/Ambil:</label>
                        <br />
                        <select name="ambil" id="ambil" className='cars'>
                            <option value="volvo">Pilih Waktu</option>
                            <option value="saab">Saab</option>
                            <option value="opel">Opel</option>
                            <option value="audi">Audi</option>
                        </select>
                    </form>

                </div>
                <div className="jumlahPenumpnag">
                    <form action="">
                        <label for="jumlahPenumpnag">Status</label>
                        <br />
                        <select name="jumlahPenumpnag" id="jumlahPenumpnag" className='cars'>
                            <option value="volvo">Disewa</option>
                            <option value="opel">Opel</option>
                            <option value="audi">Audi</option>
                        </select>
                    </form>
                </div>
                <button className='buttonSearch'>
                    <Link to="/carlist">Cari Mobil</Link>
                </button>
            </div>
        </div>
    )
}

export default SearchCar;