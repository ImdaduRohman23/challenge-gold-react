import { useState } from 'react'
import reactLogo from './assets/react.svg'
import Page1LandingPage from './pages/Page1LandingPage/Plg'
import Page2SearchCar from './pages/Page2SearchCar/SearchCar'
import Page3Carlist from './pages/Page3CarList/Carlist'
import DetailCar from './pages/Page3DetailCar/DetailCar'
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Navbar from './components/Navbar/index'
import Section7Footer from './components/Section7Footer'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <BrowserRouter>
      <Navbar />
        <Routes>
          <Route path="/" element={<Page1LandingPage />} />
          <Route path="/searchcar" element={<Page2SearchCar />} />
          <Route path="/carlist" element={<Page3Carlist />} />
          <Route path="/carlist/:id" element={<DetailCar />} />
        </Routes>
      <Section7Footer />
      </BrowserRouter>

    </div>
  )
}

export default App
